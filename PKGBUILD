# Based on community/nginx-mainline

pkgname=nginx-naxsi
_naxsirelease=1.6
pkgver=1.23.4
pkgrel=2
pkgdesc='Lightweight HTTP server, mainline release, naxsi embedded and lot of unused flags disabled'
arch=('i686' 'x86_64' 'armv6h' 'armv7h')
url='https://nginx.org'
license=('custom')
depends=('pcre2' 'zlib' 'openssl')
backup=('etc/nginx/fastcgi.conf'
        'etc/nginx/fastcgi_params'
        'etc/nginx/koi-win'
        'etc/nginx/koi-utf'
        'etc/nginx/mime.types'
        'etc/nginx/nginx.conf'
        'etc/nginx/naxsi.rules'
        'etc/nginx/scgi_params'
        'etc/nginx/uwsgi_params'
        'etc/nginx/win-utf'
        'etc/logrotate.d/nginx')
install=nginx.install
provides=('nginx')
conflicts=('nginx')
source=($url/download/nginx-$pkgver.tar.gz{,.asc}
        https://github.com/wargio/naxsi/releases/download/${_naxsirelease}/naxsi-${_naxsirelease}-src-with-deps.zip
        service
        naxsi.rules
        logrotate)
# https://nginx.org/en/pgp_keys.html
validpgpkeys=(
  'B0F4253373F8F6F510D42178520A9993A1C052F8' # Maxim Dounin <mdounin@mdounin.ru>
  '13C82A63B603576156E30A4EA0EA981B66B0D967' # Konstantin Pavlov <thresh@nginx.com>
)
sha512sums=('542a53cae32bf5c7d4d09a4940793e603e0b3c7a8a4ca2bcec84e64bc298fcf0e58297338d1ae0cd28889c4a3e359f3f48532b0addaf7d223f796ed81c3054e8'
         'SKIP'
            '4087ec1bd9929825346a088c4ede058b6dbc12ff5107bacd9bb636d8b54da574266f06b5a23a89cb1b6eec643a82f152c31a37b84d5977f6d217ccad33444350'
            '7dffe1067ea52ed69bc6dd95c4286af3b6dd13821df64d4a209b39bc5b4b46bc40566d4783695a3527ec640436e2b5e84edd41d547c3bc3ac2ef5e043bd88d66'
            '686395a280bf5b09b7dee5b2157e1680de5d41a2c73041154d2cf9a921e31dda5d3d88d7c42da0e029537a167eacb9d21e3f7adfa8e3805cabf8f8ab1247b00d'
            '57298ccaac36e2fd96cbdffeef990dcb70b80f85634e6498b878c8caeb764568a23619797a6c1548f8296e2fb0fd59c9b2f752a7844cfa200e4534fd9fbcf735')
b2sums=('fe9e4256cf092394a485f121d73f3561c0f1f3a72eaf7a279f23ca88d3cc92ae8498b895687dca582f681e621cc99906e86c7c08a3d2dfd73a203af67ce34d4a'
        'SKIP'
        'c3e2f204e132e15bb1a974f0e1c384071068f8ead81698a944bd2f1d6706b8fd0c46a8c56743dc56e243ff1a9a414b6055ea94e02096231e43f48b64ab1c4559'
        'b86bcb389df067db30679e251cc6d574ed1b94b4ece25f1ad05e018096dd718709cf755fe643d4ecd3179d3437dc044383474d3a00a384482125baae11ca0cc9'
        '460cb571a079079f6d6581d41a1d307b9c4c2aee0d951c156759c8b67015daafe2e291a4c7d0879bd6c65b1c1d8be4a86403899d30e57de7a11bac99138e2f25'
        '2eecf1d42b7ef40e8f1e943c6701ea22ba5cf7bdb94e3f07b40c7d1f2100fa21652db50f54d2b70cca9a3f16a4e5268562903e3c70c6208b8eee24307ea222e8')

_common_flags=(
  --with-file-aio
  --with-http_gunzip_module
  --with-http_gzip_static_module
  --with-http_ssl_module
  --with-http_stub_status_module
  --with-http_v2_module
  --with-pcre-jit
  --with-threads
)

_disable_flags=(
  --without-http_ssi_module
  --without-http_autoindex_module
  --without-http_geo_module
  --without-http_split_clients_module
  --without-http_scgi_module
  --without-http_uwsgi_module
  --without-http_memcached_module
  --without-http-cache
  --without-mail_pop3_module
  --without-mail_imap_module
  --without-mail_smtp_module
)

build() {
  cd $provides-$pkgver
  ./configure \
    --prefix=/etc/nginx \
    --conf-path=/etc/nginx/nginx.conf \
    --sbin-path=/usr/bin/nginx \
    --add-dynamic-module=../naxsi_src/ \
    --pid-path=/run/nginx.pid \
    --lock-path=/run/lock/nginx.lock \
    --user=http \
    --group=http \
    --http-log-path=/var/log/nginx/access.log \
    --error-log-path=stderr \
    --http-client-body-temp-path=/var/lib/nginx/client-body \
    --http-proxy-temp-path=/var/lib/nginx/proxy \
    --http-fastcgi-temp-path=/var/lib/nginx/fastcgi \
    --with-cc-opt="$CFLAGS $CPPFLAGS" \
    --with-ld-opt="$LDFLAGS" \
    ${_common_flags[@]} \
    ${_disable_flags[@]}

  make
}

package() {
  cd $provides-$pkgver
  make DESTDIR="$pkgdir" install

  sed -e 's|\<user\s\+\w\+;|user http;|g' \
    -e '44s|html|/usr/share/nginx/html|' \
    -e '54s|html|/usr/share/nginx/html|' \
    -i "$pkgdir"/etc/nginx/nginx.conf

  rm "$pkgdir"/etc/nginx/*.default

  install -d "$pkgdir"/var/lib/nginx
  install -dm700 "$pkgdir"/var/lib/nginx/proxy

  chmod 750 "$pkgdir"/var/log/nginx
  chown http:log "$pkgdir"/var/log/nginx

  install -d "$pkgdir"/usr/share/nginx
  mv "$pkgdir"/etc/nginx/html/ "$pkgdir"/usr/share/nginx

  install -Dm644 ../logrotate "$pkgdir"/etc/logrotate.d/nginx
  install -Dm644 ../service "$pkgdir"/usr/lib/systemd/system/nginx.service
  install -Dm644 LICENSE "$pkgdir"/usr/share/licenses/$provides/LICENSE
  install -Dm444 ../naxsi_rules/naxsi_core.rules "$pkgdir"/etc/nginx/naxsi_core.rules
  install -Dm444 ../naxsi.rules "$pkgdir"/etc/nginx/naxsi.rules
  install -dm700 "$pkgdir"/etc/nginx/conf.d
  install -dm700 "$pkgdir"/etc/nginx/naxsi

  rmdir "$pkgdir"/run

  install -d "$pkgdir"/usr/share/man/man8/
  gzip -9c man/nginx.8 > "$pkgdir"/usr/share/man/man8/nginx.8.gz

  for i in ftdetect ftplugin indent syntax; do
    install -Dm644 contrib/vim/$i/nginx.vim \
      "$pkgdir/usr/share/vim/vimfiles/$i/nginx.vim"
  done

  install -d "$pkgdir"/etc/nginx/ssl/
  install -dm700 "$pkgdir"/etc/nginx/ssl/
}

# vim:set ts=2 sw=2 et:
