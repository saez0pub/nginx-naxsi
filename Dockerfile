from docker.io/library/archlinux:latest AS build

RUN pacman -Syu --noconfirm base-devel git sudo gnupg pcre pcre2

RUN useradd -m -u 1000 builduser

COPY PKGBUILD service naxsi.rules logrotate nginx.install /ng_build/
RUN chown -R builduser /ng_build \
    && sudo -su builduser bash -c 'gpg --recv-key B0F4253373F8F6F510D42178520A9993A1C052F8 && gpg --recv-key 13C82A63B603576156E30A4EA0EA981B66B0D967 && cd /ng_build && makepkg' \
    && ls -l /ng_build/ \
    && ls -l /ng_build/nginx-naxsi-*.pkg.tar.zst \
    && mv $(ls /ng_build/nginx-naxsi-*.pkg.tar.zst | grep 'nginx-naxsi-[0-9]') /ng_build/nginx-naxsi.pkg.tar.zst

RUN chown root: /ng_build/nginx-naxsi.pkg.tar.zst \
    && chmod 666 /ng_build/nginx-naxsi.pkg.tar.zst

FROM docker.io/library/archlinux:latest

COPY --from=build /ng_build/nginx-naxsi.pkg.tar.zst /tmp/

RUN pacman -Syu --noconfirm pcre zlib openssl certbot\
    && pacman --noconfirm -U /tmp/nginx-naxsi.pkg.tar.zst \
    && rm /tmp/nginx-naxsi.pkg.tar.zst \
    && ln -sf /dev/stdout /var/log/nginx/access.log \
    && ln -sf /dev/stderr /var/log/nginx/error.log \
    && touch /etc/nginx/ssl.dhparam.pem \
    && chmod 0600 /etc/nginx/ssl.dhparam.pem

COPY nginx.conf helpers/http_host.conf helpers/default_headers.conf helpers/default_ssl.conf helpers/default_template.conf helpers/naxsi_server.conf /etc/nginx/
COPY start_nginx.sh /start_nginx.sh

RUN chmod +x /start_nginx.sh

CMD /start_nginx.sh
