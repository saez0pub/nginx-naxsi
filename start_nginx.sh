#!/bin/bash

function create_cert(){
  echo "creating certificate for $1"
  certbot certonly --standalone --http-01-port $HTTP_PORT --preferred-challenges http -d "$1" --email "$LETSENCRYPT_EMAIL" --agree-tos -n
}

if [ ! -f /etc/nginx/ssl.dhparam.pem ] || [ ! -s /etc/nginx/ssl.dhparam.pem ]
then
  echo initalizing dhparams
  openssl dhparam -out /etc/nginx/ssl.dhparam.pem 2048
fi

export MONITORING_PORT=${MONITORING_PORT:-81}
export HTTP_PORT=${HTTP_PORT:-80}
export HTTPS_PORT=${HTTPS_PORT:-443}
export LOG_FORMAT=${LOG_FORMAT:-upstream_time}
export DOLLAR='$'


envsubst < /etc/nginx/default_ssl.conf > /etc/nginx/default_ssl.generated.conf
envsubst < /etc/nginx/http_host.conf > /etc/nginx/conf.d/http_host.generated.conf

for conf in ${AUTOCONF}
do
  export domain="$(echo $conf | cut -d':' -f1)"
  export proxy="$(echo $conf | cut -d':' -f2-)"
  export root_directory="/var/www/${domain}"
  mkdir -p ${root_directory}
  echo "Disallow: *" > ${root_directory}/robots.txt
  echo "Generating $conf for $domain with proxy $proxy"
  envsubst < /etc/nginx/default_template.conf > /etc/nginx/conf.d/${domain}.conf
  touch /etc/nginx/naxsi/${domain}.whitelist.rules
  if [ ! -e /etc/letsencrypt/live/$domain/fullchain.pem ]
  then
    create_cert $domain
  fi
done

if [ ! -e /etc/nginx/ssl/key.pem ] || [ ! -e /etc/nginx/ssl/cert.pem ]
then
  openssl req -x509 -nodes -days 3650 -newkey rsa:2048 -keyout /etc/nginx/ssl/key.pem -out /etc/nginx/ssl/cert.pem -subj "/CN=Unsubscribe"
fi

echo "Starting nginx"
if [ -z "${AUTOCONF}" ]
then
  exec /usr/bin/nginx -g "daemon off;"
else
  exec /bin/bash -c 'oldfingerprint="$(md5sum /etc/letsencrypt/live/*/fullchain.pem | md5sum)";while :; do sleep 10s & wait ${!}; newfingerprint="$(md5sum /etc/letsencrypt/live/*/fullchain.pem | md5sum)"; if [ "$oldfingerprint" != "$newfingerprint" ]; then nginx -s reload;fi;oldfingerprint=$newfingerprint; done & nginx -g "daemon off;"'
fi
